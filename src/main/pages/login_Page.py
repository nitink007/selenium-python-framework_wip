from src.main.testBase.basePage import BasePage
from src.main.utilities.loggerUtil import customLogger

class LoginPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    log = customLogger()

    # Page level Constants
    pageName = "LoginPage"

    def clickLoginLink(self):
        self.elementClick(self.pageName, "loginLink")

    def enterEmailField(self, email):
        self.sendKeys(self.pageName, "email", email)

    def enterPasswordField(self, password):
        self.sendKeys(self.pageName, "password", password)

    def clickLoginButton(self):
        self.elementClick(self.pageName, "login")

    def verifyLoginSuccess(self):
        return self.isElementPresent(self.pageName, "loginIcon")

    def verifyLoginFailed(self):
        return self.isElementPresent(self.pageName, "loginError")

    def verifyAlreadyLoggedIn(self):
        return self.isElementPresent(self.pageName, "loginIcon")

    # test data engine need here ---------------------
    def login(self, username, passw):
        self.clickLoginLink()
        self.enterEmailField(username)
        self.enterPasswordField(passw)
        self.clickLoginButton()

    def loginApp(self):
        if self.verifyAlreadyLoggedIn() == False:
            self.log()
            self.login(username="test@email.com", passw="abcabc")
            self.log("Login Successful")
