from src.main.testBase.basePage import BasePage
from src.main.utilities.locators import Locators
from traceback import print_stack
import time


class SearchPage(BasePage):

    def __init__(self, driver):
        super().__init__(driver)
        self.driver = driver

    # Page level Constants
    pageName = "SearchPage"

    # List all the action methods

    def getCategory(self):
        self.getWebElement(self.pageName, "categoryValue")

    def setCategory(self, category):
        categoryOption = self.getWebElement(self.pageName, "categoryButton")
        categoryOption.click()
        self.getWebElement(self.pageName, "categoryOption", category).click()

    def getAuthor(self):
        return 1

    def setAuthor(self):
        return 1

    def searchCourse(self, courseName):
        try:
            self.sendKeys(self.pageName, "searchField", courseName)
            self.elementClick(self.pageName, "searchButton")
            time.sleep(3)
        except:
            print_stack()
            return False
        return True

    def verifySearchResults(self, courseName):
        status = False
        searchResultList = self.getElements(self.pageName, "searchResultListTitle")
        for searchResultItem in searchResultList:
            if courseName.lower() in ((searchResultItem.text).strip()).lower():
                print("Search result verified, Course" + courseName + " is displayed")
                status = True
        if status == False:
            print("Search result verified, Course" + courseName + " is NOT displayed")
        return status

    def verifyNoSearchResults(self, courseName):
        status = False
        searchResultList = self.getElements(self.pageName, "searchResultListTitle")
        print(len(searchResultList))
        if len(searchResultList)==0:
            status=True
        for searchResultItem in searchResultList:
            print(courseName.lower())
            print((searchResultItem.text).strip().lower())
            if courseName.lower() not in (searchResultItem.text).strip().lower():
                print("Search result verified, Course" + courseName + " is NOT displayed")
                status = True
            else:
                print("Search result verified, Course" + courseName + " is displayed")
                status = False
        return status