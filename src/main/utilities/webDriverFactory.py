"""
@package base

WebDriver Factory class implementation
It creates a webdriver instance based on browser configurations

Example:
    wdf = WebDriverFactory(browser)
    wdf.getWebDriverInstance()
"""
import traceback
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


class WebDriverFactory():

    def __init__(self, browser):
        self.browser = browser

    """
        Set chrome driver and iexplorer environment based on OS

        chromedriver = "C:/.../chromedriver.exe"
        os.environ["webdriver.chrome.driver"] = chromedriver
        self.driver = webdriver.Chrome(chromedriver)

        PREFERRED: Set the path on the machine where browser will be executed
    """

    def getWebDriverInstance(self, headless=False):
        """
       Get WebDriver Instance based on the browser configuration
        Returns:
            'WebDriver Instance'
        """
        baseURL = "https://letskodeit.teachable.com/"
        if self.browser == "ie":
            driver = webdriver.Ie(executable_path=r"C:\\Tools\\Libs\\IEDriverServer.exe")
        elif self.browser == "firefox":
            driver = webdriver.Firefox(executable_path=r"C:\\Tools\\Libs\\geckodriver.exe")
        elif self.browser == "chrome":
            if headless == True:
                chrome_options = Options()
                chrome_options.add_argument("--headless")
                chrome_options.add_argument("--window-size=1920x1080")
                driver = webdriver.Chrome(executable_path=r"C:\\Tools\\Libs\\chromedriver.exe",
                                          chrome_options=chrome_options)
            else:
                driver = webdriver.Chrome(executable_path=r"C:\\Tools\\Libs\\chromedriver.exe")
        elif self.browser == "edge":
            driver = webdriver.Edge(executable_path=r"C:\\Tools\\Libs\\MicrosoftWebDriver.exe")
        else:
            driver = webdriver.Firefox(executable_path=r"C:\\Tools\\Libs\\geckodriver.exe")
        # Setting Driver Implicit Time out for An Element
        driver.implicitly_wait(5)
        # Maximize the window
        driver.maximize_window()
        # Loading browser with App URL
        driver.get(baseURL)
        return driver
