from selenium.webdriver.common.by import By
from src.main.utilities.fileUtil import FileUtilities


class Locators():

    def __init__(self, driver):
        self.driver = driver

    def getObjectFromRepository(self):
        return 1

    def getLocatorTypeForObject(self,pageName, elementName):
        ORObject = FileUtilities.xmlORReader(pageName, elementName)
        locatorType = ORObject.get("locatorType")
        return locatorType

    def getLocatorValueForObject(self, pageName, elementName):
        ORObject = FileUtilities.xmlORReader(pageName, elementName)
        locatorValue = ORObject.get("locatorValue")
        return locatorValue


    # def getWebElement(self,elementName):
    #     locatorType = self.getLocatorTypeForObject(elementName)
    #     locatorValue = self.getLocatorValueForObject(elementName)
    #     if locatorType == "xpath":
    #         return self.driver.find_element(By.XPATH, locatorValue)

    def getModifiedORObject(self, pageName, elementName, stringToReplace):
        ORObject = FileUtilities.xmlORReader(pageName, elementName)
        originalLocatorValue = ORObject.get("locatorValue")
        modifiedLocatorValue = originalLocatorValue.replace("^stringToReplace^", stringToReplace)
        ORObject.update({"locatorValue":modifiedLocatorValue})
        return ORObject

    def getWebElement(self, pageName, elementName, stringToReplace=None):
        if stringToReplace != None:
            modifiedORObject = self.getModifiedORObject(pageName,elementName,stringToReplace)
            locatorType = modifiedORObject.get("locatorType")
            locatorValue = modifiedORObject.get("locatorValue")
            if locatorType == "xpath":
                return self.driver.find_element(By.XPATH, locatorValue)
        else:
            ORObject = FileUtilities.xmlORReader(pageName, elementName)
            locatorType = ORObject.get("locatorType")
            locatorValue = ORObject.get("locatorValue")
            if locatorType == "xpath":
                return self.driver.find_element(By.XPATH, locatorValue)


