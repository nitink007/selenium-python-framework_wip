import xml.etree.cElementTree as ET
from src.main.utilities.loggerUtil import customLogger
import os


class FileUtilities:
    log = customLogger()

    def xmlORReader(self, pagename, objectname):
        ORObject = {}
        my_path = os.path.abspath(os.path.dirname(__file__))
        path = os.path.join(my_path, "../../objectRepository/objectRepo.xml")
        OR = ET.parse(
            # r'C:\Users\Nitin\PycharmProjects\Selenium-Python-Framework_WIP\src\objectRepository\objectRepo.xml')
            path)
        root = OR.getroot()
        self.log.debug("pagename: " + pagename + " objectName: " + objectname)
        for page in root.findall('page'):
            # print(page.get('name'))
            if page.get('name') == pagename:
                for uiobject in page.find('uiElements').findall('uiobject'):
                    if uiobject.get('name') == objectname:
                        ORObject.update({'locatorType': uiobject.get('type')})
                        ORObject.update({'locatorValue': uiobject.find('locator').text})
                        return ORObject
