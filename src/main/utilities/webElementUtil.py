from selenium.webdriver.common.by import By
from traceback import print_stack
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import *
from src.main.utilities.fileUtil import FileUtilities
from src.main.utilities.loggerUtil import customLogger
import logging


class SeleniumDriver(FileUtilities):

    log = customLogger(logging.DEBUG)

    def __init__(self, driver):
        self.driver = driver

    def getByType(self, locatorType):
        locatorType = locatorType.lower()
        if locatorType == "id":
            return By.ID
        elif locatorType == "name":
            return By.NAME
        elif locatorType == "xpath":
            return By.XPATH
        elif locatorType == "css":
            return By.CSS_SELECTOR
        elif locatorType == "classname":
            return By.CLASS_NAME
        elif locatorType == "linktext":
            return By.LINK_TEXT
        else:
            self.log.debug("Locator type " + locatorType + " not correct/supported")
        return False

    def getElementByLocator(self, locator, locatorType):
        element = None
        try:
            locatorType = locatorType.lower()
            byType = self.getByType(locatorType)
            element = self.driver.find_element(byType, locator)
            self.log.debug("Element Found with Locator: "+locator+ " and Locator type: "+locatorType)
        except:
            self.log.debug("Element NOT Found with Locator: " + locator + " and Locator type: " + locatorType)
        return element

    def getElement(self, pageName, objectName):
        element = None
        try:
            uiObject = self.xmlORReader(pageName, objectName)
            locatorType = uiObject['locatorType']
            locatorValue = uiObject['locatorValue']
            self.log.debug("Locating element with locator: " + locatorValue + " locatorType: " + locatorType)
            byType = self.getByType(locatorType)
            element = self.driver.find_element(byType, locatorValue)
            self.log.debug("Element: " + objectName + " FOUND on Page: " + pageName)
        except:
            self.log.debug("Element: " + objectName + " NOT FOUND on Page: " + pageName)
        return element

    def getElements(self, pageName, objectName):
        element = None
        try:
            uiObject = self.xmlORReader(pageName, objectName)
            locatorType = uiObject['locatorType']
            locatorValue = uiObject['locatorValue']
            self.log.debug("Locating element with locator: " + locatorValue + " locatorType: " + locatorType)
            byType = self.getByType(locatorType)
            element = self.driver.find_elements(byType, locatorValue)
            self.log.debug("Element: " + objectName + " FOUND on Page: " + pageName)
        except:
            self.log.debug("Element: " + objectName + " NOT FOUND on Page: " + pageName)
        return element

    # def elementClick(self, locator, locatorType):
    #     try:
    #         element = self.getElement(locator, locatorType)
    #         element.click()
    #         print("Clicked on element with locator: " + locator + " locatorType: " + locatorType)
    #     except:
    #         print("Cannot click on the element with locator: " + locator + " locatorType: " + locatorType)
    #         print_stack()

    def elementClick(self, pageName, objectName):
        try:
            element = self.getElement(pageName, objectName)
            element.click()
            self.log.debug("Clicked on element: " + objectName + " on page: " + pageName)
        except:
            self.log.debug("Not Clicked on element: " + objectName + " on page: " + pageName)
            print_stack()

    def sendKeys(self, pageName, objectName, data):
        try:
            element = self.getElement(pageName, objectName)
            element.clear()
            element.send_keys(data)
            self.log.debug("Clicked on element: " + objectName + " on page: " + pageName)
        except:
            self.log.debug("Not Clicked on element: " + objectName + " on page: " + pageName)
            print_stack()

    def isElementPresent(self, pageName, objectName):
        try:
            element = self.getElement(pageName,objectName)
            if element is not None:
                self.log.debug("Element: " + objectName + " FOUND on Page: " + pageName)
                return True
            else:
                self.log.debug("Element: " + objectName + " NOT FOUND on Page: " + pageName)
                return False
        except:
            self.log.debug("Element: " + objectName + " FOUND on Page: " + pageName)
            return False

    def elementPresenceCheck(self, locator, byType):
        try:
            elementList = self.driver.find_elements(byType, locator)
            if len(elementList) > 0:
                self.log.debug("Element Found")
                return True
            else:
                self.log.debug("Element not found")
                return False
        except:
            self.log.debug("Element not found")
            return False

    def waitForElement(self, locator, locatorType, timeout=10, pollFrequency=0.5):
        element = None
        try:
            byType = self.getByType(locatorType)
            self.log.debug("Waiting for maximum :: " + str(timeout) +
                  " :: seconds for element to be clickable")
            wait = WebDriverWait(self.driver, 10, poll_frequency=1,
                                 ignored_exceptions=[NoSuchElementException,
                                                     ElementNotVisibleException,
                                                     ElementNotSelectableException])
            element = wait.until(EC.element_to_be_clickable((byType,
                                                             "stopFilter_stops-0")))
            self.log.debug("Element appeared on the web page")
        except:
            self.log.debug("Element not appeared on the web page")
            print_stack()
        return element
