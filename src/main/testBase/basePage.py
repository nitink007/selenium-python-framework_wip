from src.main.utilities.webElementUtil import SeleniumDriver
from src.main.utilities.commonUtil import CommonUtilities
from src.main.utilities.locators import Locators


class BasePage(SeleniumDriver,Locators):

    def __init__(self, driver):
        # super(BasePage,self).__init__(driver)
        super().__init__(driver)
        self.driver = driver
        self.commonUtil = CommonUtilities()

    # Page level Constants
    pageName = "BasePage"

    def getTitle(self):
        return self.driver.title

    def verifyPageTitle(self, titleToVerify):
        try:
            actualTitle = self.getTitle()
            return self.commonUtil.verifyTextContains(actualTitle,titleToVerify)
        except:
            return False

    def refreshPage(self):
        self.driver.refresh()
        return True