from src.main.pages.login_Page import LoginPage
import pytest

@pytest.mark.usefixtures("setUp")
class TestLogin:

    @pytest.fixture(autouse=True)
    def classSetUp(self, oneTimeSetUp):
        self.lp = LoginPage(oneTimeSetUp)
        print("Page class object instantiated")

    # @pytest.mark.skip
    @pytest.mark.run(order=2)
    def test_validLogin(self):
        self.lp.login("test@email.com", "abcabc")
        result = self.lp.verifyLoginSuccess()
        assert result == True

    # @pytest.mark.skip
    @pytest.mark.run(order=1)
    def test_invalidLogin(self):
        self.lp.login("test@email.com", "abcabc111")
        result = self.lp.verifyLoginFailed()
        assert result == True