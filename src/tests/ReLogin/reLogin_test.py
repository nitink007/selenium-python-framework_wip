from selenium import webdriver
from src.main.pages.login_Page import LoginPage
import unittest
import pytest


# @pytest.mark.usefixtures("oneTimeSetUp", "setUp")
# class TestReLogin():
#
#     @pytest.fixture(autouse=True)
#     def classSetup(self, oneTimeSetUp):
#         self.lp = LoginPage(self.driver)
#         print("Page class object instantiated")
#
#     @pytest.mark.run(order=4)
#     def test_validLogin(self):
#         self.lp.login("test@email.com", "abcabc")
#         result = self.lp.verifyLoginSuccess()
#         assert result == True
#
#     @pytest.mark.run(order=3)
#     def test_invalidLogin(self):
#         self.lp.login("test@email.com", "abcabc111")
#         result = self.lp.verifyLoginFailed()
#         assert result == True


@pytest.mark.usefixtures("setUp")
class TestReLogin:

    @pytest.fixture(autouse=True)
    def classSetUp(self, oneTimeSetUp):
        self.lp = LoginPage(oneTimeSetUp)
        print("Page class object instantiated")

    @pytest.mark.skip()
    @pytest.mark.run(order=4)
    def test_validLogin(self):
        self.lp.login("test@email.com", "abcabc")
        result = self.lp.verifyLoginSuccess()
        assert result == True

    @pytest.mark.skip()
    @pytest.mark.run(order=3)
    def test_invalidLogin(self):
        self.lp.login("test@email.com", "abcabc111")
        result = self.lp.verifyLoginFailed()
        assert result == True