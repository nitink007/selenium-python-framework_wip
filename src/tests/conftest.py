import pytest
from src.main.utilities.webDriverFactory import WebDriverFactory


@pytest.fixture()
def setUp():
    print("Running method level setUp")
    yield
    print("Running method level tearDown")


@pytest.fixture(scope="session")
def oneTimeSetUp(request):
    browser = "chrome"
    headless = True
    print("Running one time setUp")

    def driverQuit():
        print("Running one time teardown")
        driver.quit()

    wdf = WebDriverFactory(browser)
    driver = wdf.getWebDriverInstance(headless=headless)
    request.addfinalizer(driverQuit)
    return driver

# @pytest.fixture(scope="class")
# def oneTimeSetUp(request, browser):
#     print("Running one time setUp")
#     wdf = WebDriverFactory(browser)
#     driver = wdf.getWebDriverInstance()
#     if request.cls is not None:
#         request.cls.driver = driver
#     yield driver
#     driver.quit()
#     # driver.get("https://letskodeit.teachable.com/")
#     print("Running one time tearDown")

# Below code is for Command Line arguments for browser and OS
# def pytest_addoption(parser):
#     parser.addoption("--browser")
#     parser.addoption("--osType", help="Type of operating system")
#
# @pytest.fixture(scope="session")
# def browser(request):
#     return request.config.getoption("--browser")
#
# @pytest.fixture(scope="session")
# def osType(request):
#     return request.config.getoption("--osType")
