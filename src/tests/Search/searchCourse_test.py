from src.main.pages.search_Page import SearchPage
from src.main.pages.login_Page import LoginPage
import pytest


@pytest.mark.usefixtures("setUp")
class TestSearch:

    @pytest.fixture(autouse=True)
    def classSetUp(self, oneTimeSetUp):
        try:
            driver = oneTimeSetUp
            self.search = SearchPage(driver)
            self.login = LoginPage(driver)
            self.login.loginApp()
            print("Page class object instantiated")
        except:
            print("Class instantiation failed")

    @pytest.mark.run(order=3)
    def test_searchValidCourse(self):
        self.search.searchCourse("Selenium")
        result = self.search.verifySearchResults("Selenium")
        assert result == True

    @pytest.mark.run(order=4)
    def test_searchInvalidCourse(self):
        self.search.searchCourse("KNDlfnFDKNLNssds")
        result = self.search.verifyNoSearchResults("KNDlfnFDKNLNssds")
        assert result == True

    # @pytest.mark.run(order=5)
    # def test_searchValidCourse2(self):
    #     self.search.searchCourse("Selenium")
    #     result = self.search.verifySearchResults("Selenium")
    #     assert result == True
    #
    # @pytest.mark.run(order=6)
    # def test_searchValidCourse3(self):
    #     self.search.searchCourse("java")
    #     result = self.search.verifySearchResults("java")
    #     assert result == True
    #
    # @pytest.mark.run(order=7)
    # def test_searchValidCourse4(self):
    #     self.search.searchCourse("python")
    #     result = self.search.verifySearchResults("python")
    #     assert result == True
    #
    # @pytest.mark.run(order=8)
    # def test_searchValidCourse5(self):
    #     self.search.searchCourse("Linux")
    #     result = self.search.verifySearchResults("Linux")
    #     assert result == True
    #
    # @pytest.mark.run(order=9)
    # def test_searchValidCourse6(self):
    #     self.search.searchCourse("testng")
    #     result = self.search.verifySearchResults("testng")
    #     assert result == True
    #
    # @pytest.mark.run(order=10)
    # def test_searchValidCourse7(self):
    #     self.search.searchCourse("javascript")
    #     result = self.search.verifySearchResults("javascript")
    #     assert result == True
